# What Is This Project? #

This project provides a docker image for running kubectl and helm in Gitlab CI. It includes the kubectl, helm, tiller,
and git binaries.