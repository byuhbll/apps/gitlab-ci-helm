#!/bin/bash

config_name="${1}-config"

# Try cloning the config project if the variable is defined
if [[ ! -z $CONFIG_PROJECT ]]; then
  # Try cloning the config project using the same branch as the current one
  git clone --single-branch --branch \
      $CI_COMMIT_REF_NAME https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.com/$CONFIG_PROJECT.git k8s-configmap > /dev/null || true
  
  # If the config project does not have a matching branch, clone master
  if [[ ! -d "k8s-configmap" ]]; then
    git clone --single-branch --branch master https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.com/$CONFIG_PROJECT.git k8s-configmap > /dev/null || true
  fi
fi

# If the config hasn't been cloned yet, clone from the config group
if [[ ! -d "k8s-configmap" ]]; then
  git clone --single-branch --branch $CI_COMMIT_REF_NAME https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.com/$CONFIG_GROUP/$CI_PROJECT_NAME.git k8s-configmap > /dev/null || true
fi

# If the config group project doesn't have a matching branch, try master
if [[ ! -d "k8s-configmap" ]]; then
  git clone --single-branch --branch master https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.com/$CONFIG_GROUP/$CI_PROJECT_NAME.git k8s-configmap > /dev/null || true
fi

# If none of those config locations worked, return an empty config name
if [[ ! -d "k8s-configmap" ]]; then
  echo ""
  exit
fi

# If we're not deploying to production, delete the production config.
if [[ $CI_ENVIRONMENT_SLUG != "production" ]]; then
  rm -f k8s-configmap/application-prod.yml
fi

kubectl -n $KUBE_NAMESPACE create secret generic $config_name --from-file=k8s-configmap/ -o yaml --dry-run | kubectl replace -n $KUBE_NAMESPACE --force -f - > /dev/null

echo $config_name
