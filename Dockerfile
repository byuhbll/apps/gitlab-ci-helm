FROM registry.access.redhat.com/ubi7/ubi

ARG HELM_RELEASE_URL="https://get.helm.sh/helm-v2.17.0-linux-amd64.tar.gz"
ARG KUBECTL_RELEASE_VERSION="v1.11.8"
ARG OC_RELEASE_URL="https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz"

RUN yum install -y git && yum clean all

RUN cd /usr/local/bin && \
  curl -LJ -o helm.tar.gz ${HELM_RELEASE_URL} && \
  tar --strip-components=1 -xzvf helm.tar.gz && \
  rm -f helm.tar.gz

RUN cd /usr/local/bin && \
  curl -LOJ https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_RELEASE_VERSION}/bin/linux/amd64/kubectl && \
  chmod a+x kubectl && \
  curl -Lo origin-client-tools.tar.gz $OC_RELEASE_URL && \
  mkdir origin && \
  tar -xzvf origin-client-tools.tar.gz --directory origin --strip-components=1 && \
  mv origin/oc ./ && \
  chmod a+x oc && \
  rm -rf origin && \
  rm -f origin-client-tools.tar.gz

COPY scripts/create_config /usr/local/bin/
COPY scripts/create_env /usr/local/bin/
COPY scripts/create_secret /usr/local/bin/
COPY scripts/deploy /usr/local/bin/
COPY scripts/deploy_name /usr/local/bin/
COPY scripts/get_replicas /usr/local/bin/
COPY scripts/start_tiller /usr/local/bin/
COPY scripts/undeploy /usr/local/bin/
